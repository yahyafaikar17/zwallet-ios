# <center> **ZWallet** </center>


![zwallet-cover](Assets/capture-simulator/main-cover.png)

### <center>_E-wallet app that make your daily transaction easier_ </center> 
_<center> by: Yahya Faikar Hanif </center>_

<br>

## **#About This Project**
Zwallet is an E-Wallet/digital wallet application built using the Swift programming language as a native iOS app. In this project, VIPER is used as the software architecture, so that this application can be easy to maintain and perform testing.<br>

<br>

## **#Features**
- Login and Sign Up (Authentication)
- Security using One Time Password (OTP) and PIN
- Transfer transaction
- Searching contact or receiver
- History transaction

<br>

## **#Technology**
1. Swift 5 and Xcode 12.4 - A native way to build iOS and other Apple Platform
2. VIPER Design Pattern - Clean architecture to build medium to big apps, easy to test and easy to maintain
3. Cocoapods - Dependency manager for Swift

### **Dependencies**
1. Moya - Connecting the front-end to the back-end
2. Kingfisher - Displaying image from URL
3. Netfox - Help developer to debug the HTTP response

<br>

## **#How To Run This App**
1. Make sure you have Mac or Other Apple Device that can run Xcode
2. Clone this repository to your local hard drive
```sh
git clone git@gitlab.com:yahyafaikar17/zwallet-ios.git 
``` 
3. Open the App folder, then double click the Zwallet.xcworkspace
4. Just run the app and ready to use

<br>

## **#User Flow For This App**
#### **Create New User**
1. [Login Screen](#login-screen)
2. [Sign Up](#sign-up-screen)
3. Fill the 6 digits [OTP](#otp-screen)
4. Back to [Login](#login-screen)
5. [Create your 6 digits pin.](#create-pin-screen) You can only click confirm when the pin is 6 digits
6. Safely Landed at [Home Screen](#new-user-screen)

<br>

#### **Create New Transfer**
1. Click **Transfer** in [Home Screen](#home-screen)
2. Find and click your [receiver](#receiver-screen), you can also search the receiver
3. Input your [transfer amount](#transfer-amount-screen). Your balance must be [enough](#transfer-handling-screen) to perform transfer.
4. The app will be direct you to [confirmation](#transfer-confirmation-screen)
5. Enter your [pin](#enter-pin-screen)
6. If your PIN and connection internet are stable, your transfer will be [success](#transfer-success-screen). If not stable, please [try again](#transfer-failed-screen). You will be redirect to PIN Confirmation Screen.


<br>

## **#App Screen**

### _Login Screen_
![zwallet-login](Assets/capture-simulator/login-page.png)

### _Sign Up Screen_
![zwallet-signup](Assets/capture-simulator/signup-page.png)

### _OTP Screen_
![zwallet-otp](Assets/capture-simulator/otp-page.png)

### _Create Pin Screen_
![zwallet-create-pin](Assets/capture-simulator/create-pin-page.png)

### Enter Pin Screen_
![zwallet-create-pin](Assets/capture-simulator/enter-pin-page.png)

### _Home Screen_
![zwallet-home](Assets/capture-simulator/home-page.png)

### _New User Screen_
![zwallet-home](Assets/capture-simulator/empty-history-page.png)

### _Receiver Screen_
![zwallet-receiver](Assets/capture-simulator/contact-page.png)

### _Transfer Amount Screen_
![zwallet-receiver](Assets/capture-simulator/transfer-page.png)

### _Transfer Handling Screen_
![zwallet-receiver](Assets/capture-simulator/transfer-handling.png)

### _Transfer Confirmation Screen_
![zwallet-receiver](Assets/capture-simulator/confirmation-page.png)

### _Transfer Success Screen_
![zwallet-receiver](Assets/capture-simulator/trx-success-page.png)

### _Transfer Failed Screen_
![zwallet-receiver](Assets/capture-simulator/trx-failed-page.png)