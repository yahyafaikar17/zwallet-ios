//
//  HistoryPresenterImpl.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit
import Core

class HistoryPresenterImpl: HistoryPresenter {
    func loadIncomeOnly() {
        self.interactor.filterByIncome()
    }
    
    func loadOutcomeOnly() {
        self.interactor.filterByOutcome()
    }
    
    
    let view: HistoryView
    let interactor: HistoryInteractor
    let router: HistoryRouter
    
    init(view: HistoryView, interactor: HistoryInteractor, router: HistoryRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadThisWeekHistory() {
        self.interactor.getThisWeekHistory()
    }
    
    func loadThisMonthHistory() {
        self.interactor.getThisMonthHistory()
    }
    
    func backToHome() {
        let bundle = Bundle(identifier: "com.casestudy.History")
        let vc = HistoryViewController(nibName: "History", bundle: bundle)
        self.router.navigateToHome(viewController: vc)
    }
    
    func filterByDate() {
        
    }
}

extension HistoryPresenterImpl: HistoryInteractorOutput {
    func loadedIncomeHistory(transactions: [TransactionEntity]) {
        self.view.showIncomeHistoryOnly(transactions: transactions)
    }
    
    func loadedOutcomeHistory(transactions: [TransactionEntity]) {
        self.view.showOutcomeHistoryOnly(transactions: transactions)
    }
    
    func loadedThisWeekHistory(transactions: [TransactionEntity]) {
        self.view.showThisWeekHistoryData(transactions: transactions)
    }
    
    func loadedThisMonthHistory(transactions: [TransactionEntity]) {
        self.view.showThisMonthHistoryData(transactions: transactions)
    }
    
 
}
