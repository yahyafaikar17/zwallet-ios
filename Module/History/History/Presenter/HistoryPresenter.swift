//
//  HistoryPresenter.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation

protocol HistoryPresenter {
    func loadThisWeekHistory()
    func loadThisMonthHistory()
    func backToHome()
    func filterByDate()
    func loadIncomeOnly()
    func loadOutcomeOnly()
}
