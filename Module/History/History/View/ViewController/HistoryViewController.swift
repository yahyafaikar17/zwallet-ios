//
//  HistoryViewController.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core

class HistoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var outcomeFilterButton: UIButton!
    @IBOutlet weak var incomeFilterButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var dataSource = HistoryDataSource()
    
    var presenter: HistoryPresenter?
    var delegate: BackHistoryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        
        self.presenter?.loadThisMonthHistory()
        self.presenter?.loadThisWeekHistory()
        self.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.presenter?.loadThisWeekHistory()
        self.presenter?.loadThisMonthHistory()
    }

    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ThisMonthCell", bundle: Bundle(identifier: "com.casestudy.History")), forCellReuseIdentifier: "ThisMonthCell")
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "TransactionCell")
        
        self.tableView.dataSource = self.dataSource
    }
    
    
    @IBAction func backHomeAction(_ sender: Any) {
        self.delegate?.backToHome()
    }
    
    
    @IBAction func filterOutcomeAction(_ sender: Any) {
//        self.presenter?.loadIncomeOnly()
    }
    
    @IBAction func filterIncomeAction(_ sender: Any) {
//        self.presenter?.loadOutcomeOnly()
    }
}

extension HistoryViewController: BackHistoryDelegate {
    func backToHome() {
        self.presenter?.backToHome()
    }
}

extension HistoryViewController: HistoryView {
    func showIncomeHistoryOnly(transactions: [TransactionEntity]) {
        self.loadingIndicator.isHidden = true
        self.dataSource.transactions = transactions
        self.reloadInputViews()
    }
    
    func showOutcomeHistoryOnly(transactions: [TransactionEntity]) {
        self.loadingIndicator.isHidden = true
//        self.presenter?.loadOutcomeOnly()
        self.dataSource.transactions = transactions
        self.reloadInputViews()
    }
    
    func showThisWeekHistoryData(transactions: [TransactionEntity]) {
        self.loadingIndicator.isHidden = true
        self.dataSource.transactions = transactions
        self.tableView.reloadData()
    }
    
    func showThisMonthHistoryData(transactions: [TransactionEntity]) {
        self.loadingIndicator.isHidden = true
        self.dataSource.transactions = transactions
        self.tableView.reloadData()
    }
    
    
}
