//
//  ThisMonthCell.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import UIKit

class ThisMonthCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showThisMonth() {
        titleLabel.text = "This Month"
    }
    
    func showThisWeek() {
        titleLabel.text = "This Week"
    }
    
}
