//
//  HistoryView.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core

protocol HistoryView {
    func showThisWeekHistoryData(transactions: [TransactionEntity])
    func showThisMonthHistoryData(transactions: [TransactionEntity])
    func showIncomeHistoryOnly(transactions: [TransactionEntity])
    func showOutcomeHistoryOnly(transactions: [TransactionEntity])
}
