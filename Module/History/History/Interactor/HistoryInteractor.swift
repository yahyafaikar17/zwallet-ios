//
//  HistoryInteractor.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation

protocol HistoryInteractor {
    func getThisWeekHistory()
    func getThisMonthHistory()
    func filterByIncome()
    func filterByOutcome()
}
