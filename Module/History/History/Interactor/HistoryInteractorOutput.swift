//
//  HistoryInteractorOutput.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core

protocol HistoryInteractorOutput {
    func loadedThisWeekHistory(transactions: [TransactionEntity])
    func loadedThisMonthHistory(transactions: [TransactionEntity])
    func loadedIncomeHistory(transactions: [TransactionEntity])
    func loadedOutcomeHistory(transactions: [TransactionEntity])
}
