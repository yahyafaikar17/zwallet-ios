//
//  HomeInteractorOutput.swift
//  Home
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core

protocol HomeInteractorOutput {
    func loadedUserProfileDate(userProfile: UserProfileEntity)
    func loadedTransaction(transactions: [TransactionEntity], isEmpty: Bool)
//    func loadedSuccess(isFinnish: Bool)
}
