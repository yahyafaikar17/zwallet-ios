//
//  DashboardCell.swift
//  Home
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core
import Kingfisher

class DashboardCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    var delegate: DashboardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showData(userProfile: UserProfileEntity) {
        nameLabel.text = userProfile.name
        balanceLabel.text = userProfile.balance.formatToIdr()
        phoneNumberLabel.text = userProfile.phoneNumber
        let url = URL(string: userProfile.imageUrl)
        profileImage.kf.setImage(with: url)
    }
    
    
    @IBAction func showTransactionAction(_ sender: Any) {
        self.delegate?.showAllTransaction()
    }
    
    
    @IBAction func logoutAction(_ sender: Any) {
        self.delegate?.logout()
    }
    
    
    @IBAction func transferAction(_ sender: Any) {
        self.delegate?.toTransfer()
    }
    
}
