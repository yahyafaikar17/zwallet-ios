//
//  HomeViewController.swift
//  Home
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var notFoundLabel: UILabel!
    
    var dataSource = HomeDataSource()
    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.notFoundLabel.isHidden = true
        self.setupTableView()
        self.presenter?.loadProfile()
        self.presenter?.loadTransaction()
        
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "DashboardCell", bundle: Bundle(identifier: "com.casestudy.Home")), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "TransactionCell")
        self.tableView.dataSource = self.dataSource
    }
}

extension HomeViewController: DashboardCellDelegate {
    func toTransfer() {
        self.presenter?.toReceiverPage(viewController: self)
    }
    
    func showAllTransaction() {
        
        self.presenter?.showHistory(viewController: self)
    }
    
    func logout() {
        self.presenter?.logout()
    }
}

extension HomeViewController: HomeView {
    func showUserProfileData(userProfile: UserProfileEntity) {
        self.loadingIndicator.isHidden = true
        self.dataSource.userProfile = userProfile
        self.tableView.reloadData()
    }
    
    func showTransactionData(transactions: [TransactionEntity]) {
        self.loadingIndicator.isHidden = true
        self.dataSource.transaction = transactions
        self.tableView.reloadData()
    }
    
    func showNoData() {
        self.notFoundLabel.isHidden = false
    }
}
