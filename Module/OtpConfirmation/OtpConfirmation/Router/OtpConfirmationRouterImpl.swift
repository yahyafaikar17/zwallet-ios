//
//  OtpConfirmationRouterImpl.swift
//  OtpConfirmation
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class OtpConfirmationRouterImpl {
    public static func navigateToModule(view: UIViewController, email: String) {
        let bundle = Bundle(identifier: "com.casestudy.OtpConfirmation")
        let vc = OtpConfirmationViewController(nibName: "OtpConfirmationViewController", bundle: bundle)
        
        let networkManager = AuthNetworkManagerImpl()

        let router = OtpConfirmationRouterImpl()
        let interactor = OtpConfirmationInteractorImpl(otpNetworkManager: networkManager)
        let presenter =  OtpConfirmationPresenterImpl(view: vc, interactor: interactor, router: router)

        interactor.interactorOutput = presenter

        vc.presenter = presenter
        vc.email = email

        vc.modalPresentationStyle = .fullScreen
        view.present(vc, animated: true, completion: nil)
    }
}

extension OtpConfirmationRouterImpl: OtpConfirmationRouter {
    func navigateToLogin() {
        AppRouter.shared.navigationToLogin()
    }
    
    
}
