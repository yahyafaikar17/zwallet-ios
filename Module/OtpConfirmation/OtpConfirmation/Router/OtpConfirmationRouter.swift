//
//  OtpConfirmationRouter.swift
//  OtpConfirmation
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol OtpConfirmationRouter {
    func navigateToLogin()
}
