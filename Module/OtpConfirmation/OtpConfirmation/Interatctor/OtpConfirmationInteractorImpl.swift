//
//  OtpConfirmationInteractorImpl.swift
//  OtpConfirmation
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

class OtpConfirmationInteractorImpl: OtpConfirmationInteractor {
    
    var interactorOutput: OtpConfirmationInteractorOutput?
    var otpNetworkManager: AuthNetworkManager
    
    init(otpNetworkManager: AuthNetworkManager) {
        self.otpNetworkManager = otpNetworkManager
    }
    
    func getActivationToken(email: String, otp: String) {
        self.otpNetworkManager.otpValidation(email: email, otp: otp) { (data, error) in

            if data?.status == 404 {
                self.interactorOutput?.validationStatus(isSuccess: false)
            } else {
                self.interactorOutput?.validationStatus(isSuccess: true)
            }
        }
    }
}
