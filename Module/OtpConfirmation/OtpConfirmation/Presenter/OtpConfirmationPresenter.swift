//
//  OtpConfirmationPresenter.swift
//  OtpConfirmation
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol OtpConfirmationPresenter {
    func validateToken(email: String, otp: String)
    func navigateToLogin()
}
