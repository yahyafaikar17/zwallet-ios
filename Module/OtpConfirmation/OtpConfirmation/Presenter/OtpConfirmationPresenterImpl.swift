//
//  OtpConfirmationPresenterImpl.swift
//  OtpConfirmation
//
//  Created by MacBook on 27/05/21.
//

import Foundation

class OtpConfirmationPresenterImpl: OtpConfirmationPresenter {
    
    let view: OtpConfirmationView
    let interactor: OtpConfirmationInteractor
    let router: OtpConfirmationRouter
    
    init(view: OtpConfirmationView, interactor: OtpConfirmationInteractor, router: OtpConfirmationRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func validateToken(email: String, otp: String) {
        self.interactor.getActivationToken(email: email, otp: otp)
    }
    
    func navigateToLogin() {
        self.router.navigateToLogin()
    }
}

extension OtpConfirmationPresenterImpl: OtpConfirmationInteractorOutput {
    func validationStatus(isSuccess: Bool) {
        if isSuccess {
            self.view.showSuccess()
        } else {
            self.view.showError()
        }
    }
}
