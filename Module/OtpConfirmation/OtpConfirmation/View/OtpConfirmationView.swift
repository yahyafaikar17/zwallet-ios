//
//  OtpConfirmationView.swift
//  OtpConfirmation
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol OtpConfirmationView {
//    func navigateToLogin()
    func showError()
    func showSuccess()
}
