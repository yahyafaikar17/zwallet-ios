//
//  OtpConfirmationViewController.swift
//  OtpConfirmation
//
//  Created by MacBook on 26/05/21.
//

import UIKit

class OtpConfirmationViewController: UIViewController {

    @IBOutlet weak var otpText: UITextField!
    @IBOutlet weak var confrimButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var presenter: OtpConfirmationPresenter?
    var email: String = ""
    var lineBundle = Bundle(identifier: "com.casestudy.App")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confrimButton.disableButton()
        
        otpText.borderColor = .lightGray
        otpText.borderWidth = 2
        otpText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        loadingIndicator.isHidden = true
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        let otp = otpText.text ?? ""
        self.presenter?.validateToken(email: email, otp: otp)
        loadingIndicator.isHidden = false
    }
    
    func filledOTP() {
        if otpText.text?.count ?? 0 == 6 {
            otpText.borderColor = UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black
            confrimButton.enableButton()
        } else {
            otpText.borderColor = .lightGray
            confrimButton.disableButton()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filledOTP()
    }
    
}

extension OtpConfirmationViewController: OtpConfirmationView {
    func showSuccess() {
        let alert = UIAlertController(title: "BERHASIL", message: "OTP Benar. Akun telah berhasil diaktivasi", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.loadingIndicator.isHidden = true
        self.presenter?.navigateToLogin()
    }
    
    func showError() {
        let alert = UIAlertController(title: "OTP SALAH", message: "OTP yang anda masukkan salah. Mohon input kembali", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.loadingIndicator.isHidden = true
    }
}
