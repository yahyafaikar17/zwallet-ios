//
//  LoginPresenterImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit

class LoginPresenterImpl: LoginPresenter {
    
    let view: LoginView
    let interactor: LoginInteractor
    let router: LoginRouter
    
    init(view: LoginView, interactor: LoginInteractor, router: LoginRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func login(email: String, password: String) {
        self.interactor.postLoginData(email: email, password: password)
    }
    
    func navigateToSignup(viewController: UIViewController) {
        self.router.navigateToSignUp(viewController: viewController)
    }
    
    func navigateToCreatePin(viewController: UIViewController) {
        self.router.navigateToCreatePin(viewController: viewController)
    }
    
    
}

extension LoginPresenterImpl: LoginInteractorOutput {
    func authenticationResult(isSuccess: Bool, isPinSetted: Bool) {
        if isSuccess {
            if isPinSetted {
                self.router.navigateToHome()
            } else {
                self.navigateToCreatePin(viewController: LoginViewController.init(nibName: "LoginViewController", bundle: Bundle(identifier: "com.casestudy.Login")))
            }
            
        } else {
            self.view.showError()
        }
    }
}
