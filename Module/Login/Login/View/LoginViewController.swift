//
//  LoginViewController.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var mailImage: UIImageView!
    @IBOutlet weak var passwordImage: UIImageView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var hidePassword: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var presenter: LoginPresenter?
    let imgBundle = Bundle(identifier: "com.casestudy.Signup")
    let lineBundle = Bundle(identifier: "com.casestudy.App")
    var onClick: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mailImage.image = UIImage(named: "mail_empty.png", in: imgBundle, compatibleWith: nil)
        passwordImage.image = UIImage(named: "lock_empty.png", in: imgBundle, compatibleWith: nil)
        hidePassword.setImage(UIImage(named: "eye-crossed_empty.png", in: imgBundle, compatibleWith: nil), for: .normal)
        
        loginButton.disableButton()
        emailView.addBottomBorderWithColor(color: .lightGray, width: 2)
        passwordView.addBottomBorderWithColor(color: .lightGray, width: 2)
        
        emailText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        loadingIndicator.isHidden = true
        hidePassword.tag = 0
    }
    
    @IBAction func loginAction(_ sender: Any) {
        loadingIndicator.isHidden = false
        let email: String = emailText.text ?? ""
        let password: String = passwordText.text ?? ""
        self.presenter?.login(email: email, password: password)
    }
    
    @IBAction func signupAction(_ sender: Any) {
        self.presenter?.navigateToSignup(viewController: self)
    }
    
    
    @IBAction func hidePasswordAction(_ sender: Any) {
        if onClick {
            passwordText.isSecureTextEntry = true
            hidePassword.setImage(UIImage(named: "eye-crossed_empty.png", in: imgBundle, compatibleWith: nil), for: .normal)
//            hidePassword.tag = 1
            self.onClick = false
        } else {
            passwordText.isSecureTextEntry = false
            hidePassword.setImage(UIImage(named: "eye-crossed_filled.png", in: imgBundle, compatibleWith: nil), for: .normal)
//            hidePassword.tag = 0
            self.onClick = true
        }
    }
    
    func checkLoginText() {
        if (emailText.text?.isEmpty ?? true) && (passwordText.text?.isEmpty ?? true) {
            loginButton.isEnabled = false
            loginButton.backgroundColor = UIColor.lightGray
        } else {
            loginButton.isEnabled = true
            loginButton.backgroundColor = UIColor(red: 99/255, green: 121/255, blue: 244/255, alpha: 1)
            loginButton.isUserInteractionEnabled = true
        }
    }
    
    func filledEmail() {
        if (emailText.text?.isEmpty ?? true) {
            mailImage.image = UIImage(named: "mail_empty.png", in: imgBundle, compatibleWith: nil)
            emailView.addBottomBorderWithColor(color: .lightGray, width: 2)
        } else {
            mailImage.image = UIImage(named: "mail_filled.png", in: imgBundle, compatibleWith: nil)
            emailView.addBottomBorderWithColor(color: UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black, width: 2)
        }
    }
    
    func filledPassword() {
        if (passwordText.text?.isEmpty ?? true) {
            passwordImage.image = UIImage(named: "lock_empty.png", in: imgBundle, compatibleWith: nil)
            passwordView.addBottomBorderWithColor(color: .lightGray, width: 2)
        } else {
            passwordImage.image = UIImage(named: "lock_filled.png", in: imgBundle, compatibleWith: nil)
            passwordView.addBottomBorderWithColor(color: UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black, width: 2)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filledEmail()
        filledPassword()
        if !(emailText.text?.isEmpty ?? false) && !(passwordText.text?.isEmpty ?? false) && emailText.text?.contains("@") ?? true {
            loginButton.enableButton()
        } else {
            loginButton.disableButton()
        }
    }
    
}

extension LoginViewController: LoginView {
    func showError() {
        let alert = UIAlertController(
            title: "Peringatan",
            message: "User atau Password salah, Silahkan coba lagi",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.loadingIndicator.isHidden = true
    }
}
