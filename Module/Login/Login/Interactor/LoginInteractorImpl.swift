//
//  LoginInteractorImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Core

class LoginInteractorImpl: LoginInteractor {
    
    var interactorOutput: LoginInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postLoginData(email: String, password: String) {
        // Hit API dengan mengirimkan email dan password
        self.authNetworkManager.login(email: email, password: password) { (data, error) in
            if let loginData = data {
                // menyimpan user token ke useDefault
                UserDefaultHelper.shared.set(key: .email, value: loginData.data.email)
                UserDefaultHelper.shared.set(key: .userToken, value: loginData.data.token)
                UserDefaultHelper.shared.set(key: .refreshToken, value: loginData.data.refreshToken)
                
                let tokenExpiredDate: Date = Calendar.current.date(byAdding: .second, value: loginData.data.expiredIn / 1000, to: Date()) ?? Date()
                UserDefaultHelper.shared.set(key: .userTokenExpired, value: tokenExpiredDate)
                
                if loginData.status == 200 && loginData.data.hasPin {
                    // Jika login berhasil dan pin telah diset
                    self.interactorOutput?.authenticationResult(isSuccess: true, isPinSetted: true)
                } else if loginData.status == 200 && loginData.data.hasPin == false {
                    // Jika login berhasil dan pin belum diset
                    self.interactorOutput?.authenticationResult(isSuccess: true, isPinSetted: false)
                }
            } else {
                // Memberitahu presenter jika proses gagal
                self.interactorOutput?.authenticationResult(isSuccess: false, isPinSetted: false)
            }
        }
    }
    
    
}
