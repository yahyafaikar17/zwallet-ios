//
//  LoginRouter.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit

public protocol LoginRouter {
    func navigateToHome()
    func navigateToSignUp(viewController: UIViewController)
    func navigateToCreatePin(viewController: UIViewController)
}
