//
//  TransferStatusPresenterImpl.swift
//  Transfer
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core
import UIKit

class TransferStatusPresenterImpl: TransferStatusPresenter {
    
    let view: TransferStatusView
    let router: TransferStatusRouter
    
    init(view: TransferStatusView, router: TransferStatusRouter) {
        self.view = view
        self.router = router
    }
    
    func navigateToHome() {
        self.router.navigateToHome()
    }
    
    func backToPinConfirmation(view: UIViewController, contact: ContactEntity, transfer: TransferEntity) {
        self.router.backToPinConfirmation(view: view, contact: contact, transfer: transfer)
    }
}
