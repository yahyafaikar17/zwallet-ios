//
//  TransferStatusPresenter.swift
//  Transfer
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core
import UIKit

protocol TransferStatusPresenter {
    func navigateToHome()
    func backToPinConfirmation(view: UIViewController, contact: ContactEntity, transfer: TransferEntity)
}
