//
//  TransferStatusRouterImpl.swift
//  Transfer
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import UIKit
import Core

class TransferStatusRouterImpl {
    static func navigateToModule(view: UIViewController, contact: ContactEntity, transfer: TransferEntity, isSucces: Bool) {
        let bundle = Bundle(identifier: "com.casestudy.Transfer")
        let vc = TransferStatusViewController(nibName: "TransferStatusViewController", bundle: bundle)
        
        let router = TransferStatusRouterImpl()
        
        let presenter = TransferStatusPresenterImpl(view: vc, router: router)
        
        vc.presenter = presenter
        vc.isTransferSuccess = isSucces
        vc.selectedContact = contact
        vc.transferInfo = transfer
        
        vc.modalPresentationStyle = .fullScreen
        view.present(vc, animated: true, completion: nil)
    }
}

extension TransferStatusRouterImpl: TransferStatusRouter {
    func navigateToHome() {
        AppRouter.shared.navigationToHome()
    }
    
    func backToPinConfirmation(view: UIViewController, contact: ContactEntity, transfer: TransferEntity) {
        ConfirmationRouterImpl.navigateToModule(view: view, contact: contact, transfer: transfer)
    }
}
