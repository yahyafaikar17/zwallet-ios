//
//  TransferStatusViewController.swift
//  Transfer
//
//  Created by MacBook on 30/05/21.
//

import UIKit
import Core

class TransferStatusViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    
    
    var dataSource = TransferStatusDataSource()
    var presenter: TransferStatusPresenter?
    var isTransferSuccess: Bool?
    
    var selectedContact: ContactEntity = ContactEntity(id: 0, name: "", phoneNumber: "", image: "")
    var transferInfo: TransferEntity = TransferEntity(amount: 0, balanceLeft: 0, date: "", notes: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        showTransferStatus()
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ConfirmDetailsCell", bundle: Bundle(identifier: "com.casestudy.Transfer")), forCellReuseIdentifier: "ConfirmDetailsCell")
        self.tableView.register(UINib(nibName: "SectionTitleCell", bundle: Bundle(identifier: "com.casestudy.Transfer")), forCellReuseIdentifier: "SectionTitleCell")
        self.tableView.register(UINib(nibName: "ContactCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "ContactCell")
        self.tableView.dataSource = self.dataSource
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        if self.isTransferSuccess! {
            self.presenter?.navigateToHome()
        } else {
            self.presenter?.backToPinConfirmation(view: self, contact: selectedContact, transfer: TransferEntity(amount: transferInfo.amount, balanceLeft: transferInfo.balanceLeft, date: currentDate, notes: transferInfo.notes))
        }
    }
    
}

extension TransferStatusViewController: TransferStatusView {
    func showTransferStatus() {
        if self.isTransferSuccess! {
            titleLabel.text = "Transfer Success"
            messageLabel.text = ""
            button.titleLabel?.text = "Back To Home"
            
        } else {
            titleLabel.text = "Transfer Failed"
            messageLabel.text = "We can’t transfer your money at the moment, we recommend you to check your internet connection and try again."
            iconImage.image = UIImage(named: "failed.png", in: Bundle(identifier: "com.casestudy.Transfer"), compatibleWith: nil)
            button.setTitle("Try Again", for: .normal)
        }
    }
}
