//
//  TransferStatusView.swift
//  Transfer
//
//  Created by MacBook on 30/05/21.
//

import Foundation

protocol TransferStatusView {
    func showTransferStatus()
}
