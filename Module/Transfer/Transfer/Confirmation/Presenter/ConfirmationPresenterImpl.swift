//
//  ConfirmationPresenterImpl.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core
import UIKit

class ConfirmationPresenterImpl: ConfirmationPresenter {
    
    let view: ConfirmationView
    let router: ConfirmationRouter
    
    init(view: ConfirmationView, router: ConfirmationRouter) {
        self.view = view
        self.router = router
    }
    
    func goToInputAmont(viewController: UIViewController, contact: ContactEntity) {
        self.router.navigateToInputAmount(viewController: viewController, contact: contact)
    }
    
    func goToPinConfirmation(viewController: UIViewController, contact: ContactEntity, trasnfer: TransferEntity) {
        self.router.navigateToPinConfirmation(viewController: viewController, contact: contact, trasnfer: trasnfer)
    }
}
