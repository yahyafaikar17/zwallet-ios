//
//  ConfirmationPresenter.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

protocol ConfirmationPresenter {
    func goToInputAmont(viewController: UIViewController, contact: ContactEntity)
    func goToPinConfirmation(viewController: UIViewController, contact: ContactEntity, trasnfer: TransferEntity)
}
