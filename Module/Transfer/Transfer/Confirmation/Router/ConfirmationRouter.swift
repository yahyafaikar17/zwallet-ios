//
//  ConfirmationRouter.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

protocol ConfirmationRouter {
    func navigateToPinConfirmation(viewController: UIViewController, contact: ContactEntity, trasnfer: TransferEntity)
    func navigateToInputAmount(viewController: UIViewController, contact: ContactEntity)
}
