//
//  ConfirmationRouterImpl.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

class ConfirmationRouterImpl {
    static func navigateToModule(view: UIViewController, contact: ContactEntity, transfer: TransferEntity) {
        let bundle = Bundle(identifier: "com.casestudy.Transfer")
        let vc = ConfirmationViewController(nibName: "ConfirmationViewController", bundle: bundle)
        
        let router = ConfirmationRouterImpl()
        
        let presenter = ConfirmationPresenterImpl(view: vc, router: router)
        
        vc.presenter = presenter
        vc.selectedContact = contact
        vc.transferInfo = transfer
        
        vc.modalPresentationStyle = .fullScreen
        view.present(vc, animated: true, completion: nil)
    }
}

extension ConfirmationRouterImpl: ConfirmationRouter {
    func navigateToPinConfirmation(viewController: UIViewController, contact: ContactEntity, trasnfer: TransferEntity) {
        // To Pin Confirmation Page to check pin
        PinConfirmationRouterImpl.navigateToModule(view: viewController, contact: contact, transferInfo: trasnfer)
    }
    
    func navigateToInputAmount(viewController: UIViewController, contact: ContactEntity) {
        AppRouter.shared.navigateToTransfer(viewController, contact: contact)
    }
}
