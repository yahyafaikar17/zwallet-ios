//
//  TransferEntity.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

struct TransferEntity {
    var amount: Int
    var balanceLeft: Int
    var date: String
    var notes: String
}
