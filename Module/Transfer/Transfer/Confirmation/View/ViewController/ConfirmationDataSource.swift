//
//  ConfirmationDataSource.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core
import UIKit

class ConfirmationDataSource: NSObject, UITableViewDataSource {
    
    var viewController: ConfirmationViewController!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 1, 2:
            return 1
        case 3:
            return 4
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SectionTitleCell", for: indexPath) as! SectionTitleCell
            cell.showTransferTo()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
            cell.showContactData(contact: viewController.selectedContact)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SectionTitleCell", for: indexPath) as! SectionTitleCell
            cell.showDetailsTitle()
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmDetailsCell", for: indexPath) as! ConfirmDetailsCell
            switch indexPath.row {
            case 0:
                cell.showAmount(data: viewController.transferInfo)
            case 1:
                cell.showBalanceLeft(data: viewController.transferInfo)
            case 2:
                cell.showDate(data: viewController.transferInfo)
            case 3:
                cell.showNotes(data: viewController.transferInfo)
            default:
                cell.showAmount(data: viewController.transferInfo)
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmDetailsCell", for: indexPath) as! ConfirmDetailsCell
            return cell
        }
    }
}
