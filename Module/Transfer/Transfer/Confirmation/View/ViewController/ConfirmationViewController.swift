//
//  ConfirmationViewController.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import Core

class ConfirmationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    var dataSource = ConfirmationDataSource()
    var presenter: ConfirmationPresenter?
    
    // Data in this view
    var selectedContact: ContactEntity = ContactEntity(id: 0, name: "", phoneNumber: "", image: "")
    var transferInfo: TransferEntity = TransferEntity(amount: 0, balanceLeft: 0, date: "", notes: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.presenter.
        setupTableView()
        // Do any additional setup after loading the view.
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ConfirmDetailsCell", bundle: Bundle(identifier: "com.casestudy.Transfer")), forCellReuseIdentifier: "ConfirmDetailsCell")
        self.tableView.register(UINib(nibName: "SectionTitleCell", bundle: Bundle(identifier: "com.casestudy.Transfer")), forCellReuseIdentifier: "SectionTitleCell")
        self.tableView.register(UINib(nibName: "ContactCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "ContactCell")
        self.tableView.dataSource = self.dataSource
    }
    
    @IBAction func backToAmountAction(_ sender: Any) {
        self.presenter?.goToInputAmont(viewController: self, contact: selectedContact)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        self.presenter?.goToPinConfirmation(viewController: self, contact: selectedContact, trasnfer: transferInfo)
    }
}

extension ConfirmationViewController: ConfirmationView {
    func showAmount() {
//        
    }
    
    
}
