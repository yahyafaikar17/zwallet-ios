//
//  ConfirmDetailsCell.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import UIKit

class ConfirmDetailsCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showAmount(data: TransferEntity) {
        titleLabel.text = "Amount"
        detailsLabel.text = data.amount.formatToIdr()
    }
    
    func showBalanceLeft(data: TransferEntity) {
        titleLabel.text = "Balance Left"
        detailsLabel.text = data.balanceLeft.formatToIdr()
    }
    
    func showDate(data: TransferEntity) {
        titleLabel.text = "Date & Time"
        detailsLabel.text = data.date
    }
    
    func showNotes(data: TransferEntity) {
        titleLabel.text = "Notes"
        detailsLabel.text = data.notes
    }
    
}
