//
//  SectionTitleCell.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import UIKit

class SectionTitleCell: UITableViewCell {

    @IBOutlet weak var sectionTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showTransferTo() {
        sectionTitleLabel.text = "Transfer to"
    }
    
    func showDetailsTitle() {
        sectionTitleLabel.text = "Details"
    }
}
