//
//  PinConfirmationPresenter.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

protocol PinConfirmationPresenter {
    func goToTransferSuccess(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
    func goToTransferFailed(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
    func backToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
    func checkPin(pin: String)
    func checkTransfer(id: Int, amount: Int, notes: String, pin: String)
}
