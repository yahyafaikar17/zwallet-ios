//
//  PinConfirmationPresenterImpl.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core
import UIKit

class PinConfirmationPresenterImpl: PinConfirmationPresenter {
    
    var view: PinConfirmationView
    let interactor: PinConfirmationInteractor
    let router: PinConfirmationRouter
    
    init(view: PinConfirmationView, interactor: PinConfirmationInteractor, router: PinConfirmationRouter) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    func goToTransferSuccess(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        self.router.navigateToTransferSuccess(viewController: viewController, contact: contact, transferInfo: transferInfo)
    }
    
    func backToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        self.router.navigateToConfirmation(viewController: viewController, contact: contact, transferInfo: transferInfo)
    }
    
    func checkPin(pin: String) {
        self.interactor.checkPin(pin: pin)
    }
    
    func checkTransfer(id: Int, amount: Int, notes: String, pin: String) {
        self.interactor.transfer(id: id, amount: amount, notes: notes, pin: pin)
    }
    
    func goToTransferFailed(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        self.router.navigateToTransferFailed(viewController: viewController, contact: contact, transferInfo: transferInfo)
    }
    
}

extension PinConfirmationPresenterImpl: PinConfirmationOutputInteractor {
    func pinConfirmationResult(isSuccess: Bool) {
        if isSuccess {
            self.view.checkTransferResult()
        } else {
            self.view.showError()
        }
    }
    
    func transferResult(isSuccess: Bool) {
        if isSuccess {
            self.view.goToTransferSuccess()
        } else {
            self.view.goToTransferFailed()
        }
    }
    
}
