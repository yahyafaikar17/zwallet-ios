//
//  PinConfirmationInteractor.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

protocol PinConfirmationInteractor {
    func checkPin(pin: String)
    func transfer(id: Int, amount: Int, notes: String, pin: String)
}
