//
//  PinConfirmationInteractorImpl.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

class PinConfirmationInteractorImpl: PinConfirmationInteractor {
    
    
    
    var interactorOutput: PinConfirmationOutputInteractor?
    
    let pinNetworkManager: PinNetworkManager
    let transferNetworkManager: TransferNetworkManager
    
    init(pinNetworkManager: PinNetworkManager, transferNetworkManager: TransferNetworkManager) {
        self.pinNetworkManager = pinNetworkManager
        self.transferNetworkManager = transferNetworkManager
    }
    
    func checkPin(pin: String) {
        self.pinNetworkManager.checkPin(pin: pin) { (data, error) in
            if let checkPin = data {
                if checkPin.status == 200 {
                    self.interactorOutput?.pinConfirmationResult(isSuccess: true)
                } else {
                    self.interactorOutput?.pinConfirmationResult(isSuccess: false)
                }
            } else {
                self.interactorOutput?.pinConfirmationResult(isSuccess: false)
            }
        }
    }
    
    func transfer(id: Int, amount: Int, notes: String, pin: String) {
        self.transferNetworkManager.performTransfer(id: id, amount: amount, notes: notes, pin: pin) { (data, error) in
            if let transferResponse = data {
                if transferResponse.status == 200 {
                    self.interactorOutput?.transferResult(isSuccess: true)
                } else {
                    self.interactorOutput?.transferResult(isSuccess: false)
                }
            }
        }
    }
}
