//
//  PinConfirmationOutputInteractor.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PinConfirmationOutputInteractor {
    func pinConfirmationResult(isSuccess: Bool)
    func transferResult(isSuccess: Bool)
}
