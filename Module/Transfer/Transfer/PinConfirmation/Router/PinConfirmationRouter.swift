//
//  PinConfirmationRouter.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

protocol PinConfirmationRouter {
    func navigateToTransferSuccess(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
    func navigateToTransferFailed(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
    func navigateToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
}
