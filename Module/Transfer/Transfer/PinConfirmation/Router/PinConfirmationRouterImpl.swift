//
//  PinConfirmationRouterImpl.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

class PinConfirmationRouterImpl {
    static func navigateToModule(view: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        
        let bundle = Bundle(identifier: "com.casestudy.Transfer")
        let vc = PinConfirmationViewController(nibName: "PinConfirmationViewController", bundle: bundle)
        
        let pinNetworkManager = PinNetworkManagerImpl()
        let transferNetworkManager = TransferNetworkManagerImpl()
        let router = PinConfirmationRouterImpl()
        
        let interactor = PinConfirmationInteractorImpl(pinNetworkManager: pinNetworkManager, transferNetworkManager: transferNetworkManager)
        
        let presenter = PinConfirmationPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        vc.selectedContact = contact
        vc.transferInfo = transferInfo
        
        vc.modalPresentationStyle = .fullScreen
        
        view.present(vc, animated: true, completion: nil)
    }
}

extension PinConfirmationRouterImpl: PinConfirmationRouter {
    func navigateToTransferFailed(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        TransferStatusRouterImpl.navigateToModule(view: viewController, contact: contact, transfer: transferInfo, isSucces: false)
    }
    
    func navigateToTransferSuccess(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        TransferStatusRouterImpl.navigateToModule(view: viewController, contact: contact, transfer: transferInfo, isSucces: true)
    }
    
    func navigateToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        ConfirmationRouterImpl.navigateToModule(view: viewController, contact: contact, transfer: transferInfo)
    }
}
