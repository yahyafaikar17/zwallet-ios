//
//  PinConfirmationViewController.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import Core

class PinConfirmationViewController: UIViewController {

    @IBOutlet weak var pinText: UITextField!
    @IBOutlet weak var transferButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var presenter: PinConfirmationPresenter?
    var selectedContact: ContactEntity = ContactEntity(id: 0, name: "", phoneNumber: "", image: "")
    var transferInfo: TransferEntity = TransferEntity(amount: 0, balanceLeft: 0, date: "", notes: "")
    var lineBundle = Bundle(identifier: "com.casestudy.App")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        transferButton.disableButton()
        
        pinText.borderColor = .lightGray
        pinText.borderWidth = 2
        pinText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        loadingIndicator.isHidden = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.presenter?.backToConfirmation(viewController: self, contact: selectedContact, transferInfo: transferInfo)
    }
    
    @IBAction func transferAction(_ sender: Any) {
        let checkPin: String = pinText.text ?? ""
        self.presenter?.checkPin(pin: checkPin)
        loadingIndicator.isHidden = false
    }
    
    func filledPIN() {
        if pinText.text?.count ?? 0 == 6 {
            pinText.borderColor = UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black
            transferButton.enableButton()
        } else {
            pinText.borderColor = .lightGray
            transferButton.disableButton()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filledPIN()
    }
    
}

extension PinConfirmationViewController: PinConfirmationView {
    func goToTransferSuccess() {
        self.presenter?.goToTransferSuccess(viewController: self, contact: selectedContact, transferInfo: transferInfo)
    }
    
    func goToTransferFailed() {
        self.presenter?.goToTransferFailed(viewController: self, contact: selectedContact, transferInfo: transferInfo)
    }
    
    func showError() {
        let alert = UIAlertController(title: "PIN SALAH", message: "PIN yang anda masukkan salah, mohon untuk input kembali.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.modalPresentationStyle = .popover
        present(alert, animated: true, completion: nil)
        loadingIndicator.isHidden = true
    }
    
    func checkTransferResult() {
        self.presenter?.checkTransfer(id: selectedContact.id, amount: transferInfo.amount, notes: transferInfo.notes, pin: pinText.text ?? "")
    }
}
