//
//  PinConfirmationView.swift
//  Transfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PinConfirmationView {
    func showError()
    func goToTransferSuccess()
    func goToTransferFailed()
    func checkTransferResult()
}
