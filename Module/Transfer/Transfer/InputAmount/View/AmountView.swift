//
//  AmountView.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol AmountView {
//    var balance: Int { get set}
    func showCurrentBalance(balance: Int)
    func showSelectedContact(contact: ContactEntity)
}
