//
//  AmountViewController.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core

class AmountViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userBalanceLeft: UILabel!
    @IBOutlet weak var amountText: UITextField!
    @IBOutlet weak var notesText: UITextField!
    @IBOutlet weak var notesImage: UIImageView!
    @IBOutlet weak var notesView: UIView!
    @IBOutlet weak var notEnoughLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var dataSource = AmountDataSource()
    var presenter: AmountPresenter?
    var balance: Int = 0
    let imgBundle = Bundle(identifier: "com.casestudy.Transfer")
    let lineBundle = Bundle(identifier: "com.casestudy.App")
    
    // Contact Selected Table Data
    var contactSelected: ContactEntity = ContactEntity(id: 0, name: "", phoneNumber: "", image: "")
    var transferInfo: TransferEntity = TransferEntity(amount: 0, balanceLeft: 0, date: "", notes: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTable()
        self.presenter?.loadedBalance()
        
        confirmButton.disableButton()
        notesView.addBottomBorderWithColor(color: .lightGray, width: 2)
        notesText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        notEnoughLabel.isHidden = true
        amountText.addTarget(self, action: #selector(checkAmountChange(_:)), for: .editingChanged)
    }
    
    func setupTable() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ContactCell", bundle: Bundle( identifier: "com.casestudy.Core")), forCellReuseIdentifier: "ContactCell")
        self.tableView.dataSource = self.dataSource
    }
    
    @IBAction func toReceiverAction(_ sender: Any) {
        self.presenter?.goToReceiver(viewController: self)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        let amountInt = Int(amountText.text?.collectNumberOnly() ?? "0") ?? 0
        transferInfo.amount = amountInt
        transferInfo.balanceLeft = balance - amountInt
        transferInfo.date = Date().id_format()
        transferInfo.notes = notesText.text ?? "0"
        self.presenter?.goToConfirmation(viewController: self, contact: contactSelected, transferInfo: transferInfo)
    }
    
    func filledNotes() {
        if (notesText.text?.isEmpty ?? true) {
            notesImage.image = UIImage(named: "note_empty.png", in: imgBundle, compatibleWith: nil)
            notesView.addBottomBorderWithColor(color: .lightGray, width: 2)
        } else {
            notesImage.image = UIImage(named: "note_filled.png", in: imgBundle, compatibleWith: nil)
            notesView.addBottomBorderWithColor(color: UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black, width: 2)
        }
    }
    
    func filledBalance() {
        if amountText.text?.isEmpty ?? true {
            confirmButton.disableButton()
        } else {
            confirmButton.enableButton()
        }
    }
    
    func checkBalance() {
        let amountInt = Int(amountText.text?.collectNumberOnly() ?? "0") ?? 0
        transferInfo.amount = amountInt
        transferInfo.balanceLeft = balance - amountInt
        if transferInfo.balanceLeft < 0 {
            notEnoughLabel.isHidden = false
            confirmButton.disableButton()
        } else {
            notEnoughLabel.isHidden = true
            confirmButton.enableButton()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filledNotes()
    }
    
    @objc func checkAmountChange(_ textField: UITextField) {
        if amountText.text?.isEmpty ?? true {
            filledBalance()
        } else {
            checkBalance()
        }
    }
}

extension AmountViewController: AmountView {
    
    func showCurrentBalance(balance: Int) {
        loadingIndicator.isHidden = true
        self.balance = balance
        userBalanceLeft.text = "\(balance.formatToIdr()) Available"
    }
    
    func showSelectedContact(contact: ContactEntity) {
        self.dataSource.contactInfo = contact
        self.tableView.reloadData()
    }
}
