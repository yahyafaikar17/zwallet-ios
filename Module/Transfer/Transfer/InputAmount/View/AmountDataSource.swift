//
//  AmountDataSource.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

class AmountDataSource: NSObject, UITableViewDataSource {
    
    var viewController: AmountViewController!
    var contactInfo: ContactEntity = ContactEntity(id: 0, name: "", phoneNumber: "", image: "")
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        cell.showContactData(contact: viewController.contactSelected)
        return cell
    }
}
