//
//  AmountOutputInteractor.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation

protocol AmountOutputInteractor {
    func loadedBalance(balance: Int)
}
