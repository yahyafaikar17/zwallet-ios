//
//  AmountInteractorImpl.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

class AmountInteractorImpl: AmountInteractor {
    
    var interactorOutput: AmountOutputInteractor?
    
    let amountNetworkManager: BalanceNetworkManager
    
    init(amountNetworkManager: BalanceNetworkManager) {
        self.amountNetworkManager = amountNetworkManager
    }
    
    func getBalance() {
        self.amountNetworkManager.getBalance { (data, error) in
            if let balance = data {
                let userBalance = BalanceEntity(balance: balance.balance)
                self.interactorOutput?.loadedBalance(balance: userBalance.balance)
            }
        }
    }
}
