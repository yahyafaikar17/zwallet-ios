//
//  AmountRouter.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

protocol AmountRouter {
    func navigateToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
    func navigateToContacts(viewController: UIViewController)
}
