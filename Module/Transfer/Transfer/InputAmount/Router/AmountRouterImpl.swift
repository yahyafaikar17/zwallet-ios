//
//  AmountRouterImpl.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

public class AmountRouterImpl {
    public static func navigateToModule(view: UIViewController, contact: ContactEntity) {
        let bundle = Bundle(identifier: "com.casestudy.Transfer")
        let vc = AmountViewController(nibName: "AmountViewController", bundle: bundle)
        
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        let router = AmountRouterImpl()
        let interactor = AmountInteractorImpl(amountNetworkManager: balanceNetworkManager)
        
        let presenter = AmountPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        vc.contactSelected = contact
        
        vc.modalPresentationStyle = .fullScreen
        
        view.present(vc, animated: true, completion: nil)
    }
    
}

extension AmountRouterImpl: AmountRouter {
    func navigateToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        ConfirmationRouterImpl.navigateToModule(view: viewController, contact: contact, transfer: transferInfo)
    }
    
    func navigateToContacts(viewController: UIViewController) {
        AppRouter.shared.navigateToReceiver(viewController)
    }
    
    
}
