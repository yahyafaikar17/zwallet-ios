//
//  AmountPresenter.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

protocol AmountPresenter {
    func loadedBalance()
    func goToReceiver(viewController: UIViewController)
    func goToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity)
}
