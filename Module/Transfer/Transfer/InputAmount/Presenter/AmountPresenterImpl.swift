//
//  AmountPresenterImpl.swift
//  Transfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

class AmountPresenterImpl: AmountPresenter {
       
    var view: AmountView
    let interactor: AmountInteractor
    let router: AmountRouter
    
    init(view: AmountView, interactor: AmountInteractor, router: AmountRouter) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    func loadedBalance() {
        self.interactor.getBalance()
    }
    
    func goToReceiver(viewController: UIViewController) {
        self.router.navigateToContacts(viewController: viewController)
    }
    
    func goToConfirmation(viewController: UIViewController, contact: ContactEntity, transferInfo: TransferEntity) {
        self.router.navigateToConfirmation(viewController: viewController, contact: contact, transferInfo: transferInfo)
    }
}

extension AmountPresenterImpl: AmountOutputInteractor {
    func loadedBalance(balance: Int) {
//        self.view.balance = balance
        self.view.showCurrentBalance(balance: balance)
    }
}
