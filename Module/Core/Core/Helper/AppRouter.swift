//
//  AppRouter.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit

public class AppRouter {
    public static let shared: AppRouter = AppRouter()
    
    public var loginScene: (() -> ())? = nil
    
    public func navigationToLogin() {
        self.loginScene?()
    }
    
    public var homeScene: (() -> ())? = nil
    public func navigationToHome() {
        self.homeScene?()
    }
    
    public var historyScene: ((_ viewController: UIViewController) -> ())? = nil
    public func navigateToHistory(_ viewController: UIViewController) {
        self.historyScene?(viewController)
    }
    
    public var signupScene: ((_ viewController: UIViewController) -> ())? = nil
    public func navigateToSignup(_ viewController: UIViewController) {
        self.signupScene?(viewController)
    }
    
    public var createPinScene: (() -> ())? = nil
    public func navigateToCreatePin() {
        self.createPinScene?()
    }
    
    public var otpConfirmationScene: ((_ viewController: UIViewController, _ email: String) -> ())? = nil
    public func navigateToOTP(_ viewController: UIViewController, email: String) {
        self.otpConfirmationScene?(viewController, email)
    }
    
    public var receiverScene: ((_ viewController: UIViewController) -> ())? = nil
    public func navigateToReceiver(_ viewController: UIViewController) {
        self.receiverScene?(viewController)
    }
    
    public var transferScene: ((_ viewController: UIViewController, _ contact: ContactEntity) -> ())? = nil
    public func navigateToTransfer(_ viewController: UIViewController, contact: ContactEntity) {
        self.transferScene?(viewController, contact)
    }
}
