//
//  ContactCell.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Kingfisher

public class ContactCell: UITableViewCell {
    
    public var delegate: ContactCellDelegate?
    var imgUrl: String = ""
    var contactId: Int = 0

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        cellView.addGestureRecognizer(tap)
        
        adjustFontScale(label: nameLabel)
        adjustFontScale(label: phoneNumberLabel)
    }

    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func adjustFontScale(label: UILabel) {
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
    }
    
    public func showContactData(contact: ContactEntity) {
        self.nameLabel.text = contact.name
        self.phoneNumberLabel.text = contact.phoneNumber
        let url = URL(string: contact.image)
        imgUrl = contact.image
        contactId = contact.id
        self.profileImage.kf.setImage(with: url)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.delegate?.navigateToTransfer(contact: ContactEntity.init(id: contactId, name: self.nameLabel.text ?? "", phoneNumber: self.phoneNumberLabel.text ?? "", image: imgUrl))
    }
        
        
}
