//
//  TransactionCell.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Kingfisher

public class TransactionCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        adjustFontScale(label: userNameLabel)
//        adjustFontScale(label: notesLabel)
//        adjustFontScale(label: amountLabel)
    }

    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func adjustFontScale(label: UILabel) {
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
    }

    
    public func showData(transaction: TransactionEntity) {
        self.userNameLabel.text = transaction.name
        self.notesLabel.text = transaction.notes
        let url = URL(string: transaction.imageUrl)
        self.userImage.kf.setImage(with: url)
        if transaction.type == "in" {
            self.amountLabel.text = "+ \(transaction.amount.formatToIdr())"
            self.amountLabel.textColor = #colorLiteral(red: 0.1176470588, green: 0.7568627451, blue: 0.3725490196, alpha: 1)
        } else {
            self.amountLabel.text = "- \(transaction.amount.formatToIdr())"
            self.amountLabel.textColor = #colorLiteral(red: 1, green: 0.3568627451, blue: 0.2156862745, alpha: 1)
        }
    }
    
}
