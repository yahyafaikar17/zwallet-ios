//
//  ContactCellDelegate.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation

public protocol ContactCellDelegate {
    func navigateToTransfer(contact: ContactEntity)
}
