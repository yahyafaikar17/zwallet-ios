//
//  SignupNetworkManagerImpl.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Moya

class SignupNetworkManagerImpl: SignupNetworkManager {
    func signup(email: String, username: String, password: String, completion: @escaping (SignupResponse?, Error?) -> ()) {
        let provider = MoyaProvider<SignupApi>()
        provider.request(.signup(email: email, username: username, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let signupResponse = try decoder.decode(SignupResponse.self, from: result.data)
                    completion(signupResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
    init() {
        
    }
}
