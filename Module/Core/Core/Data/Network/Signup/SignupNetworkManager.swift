//
//  SignupNetworkManager.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol SignupNetworkManager {
    func signup(email: String, username: String, password: String, completion: @escaping (SignupResponse?, Error?) -> ())
}
