//
//  PinNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public class PinNetworkManagerImpl: PinNetworkManager {
    
    public init() {
            
    }
    
    public func create(pin: String, completion: @escaping (CreatePinResponse?, Error?) -> ()) {
        let provider = MoyaProvider<PinApi>(isRefreshToken: true)
        provider.request(.create(pin: pin)) { (response) in
            switch response {
            case .success(let result):
                let decocder = JSONDecoder()
                do {
                    let pinResponse = try decocder.decode(CreatePinResponse.self, from: result.data)
                    completion(pinResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func checkPin(pin: String, completion: @escaping (CreatePinResponse?, Error?) -> ()) {
        let provider = MoyaProvider<PinApi>(isRefreshToken: true)
        provider.request(.checkPin(pin: pin)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let pinResponse = try decoder.decode(CreatePinResponse.self, from: result.data)
                    completion(pinResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
}
