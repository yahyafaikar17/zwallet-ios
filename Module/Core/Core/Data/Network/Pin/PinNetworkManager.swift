//
//  PinNetworkManager.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public protocol PinNetworkManager{
    func create(pin: String, completion: @escaping (CreatePinResponse?, Error?) -> ())
    func checkPin(pin: String, completion: @escaping (CreatePinResponse?, Error?) -> ())
}
