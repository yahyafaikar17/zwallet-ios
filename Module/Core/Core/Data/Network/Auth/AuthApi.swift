//
//  AuthApi.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Moya

public enum AuthApi {
    case login(email: String, password: String)
    case otpValidation(email: String, otp: String)
    case refreshToken(email: String, refreshToken: String)
    case signup(email: String, username: String, password: String)
}

extension AuthApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var path: String {
        switch self {
        case .login:
            return "/auth/login"
        case .otpValidation(let email, let otp):
            return "/auth/activate/\(email)/\(otp)"
        case .refreshToken:
            return "/auth/refresh-token"
        case .signup:
            return "/auth/signup"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .login, .refreshToken, .signup:
            return .post
        case .otpValidation:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .login(let email, let password):
            return .requestParameters(
                parameters: [
                    "email": email,
                    "password": password],
                encoding: JSONEncoding.default
            )
        case .otpValidation:
            return .requestPlain
        case .refreshToken(let email, let refreshToken):
            return .requestParameters(parameters: [
                                        "email": email,
                                        "refreshToken": refreshToken],
                                      encoding: JSONEncoding.default)
        case .signup(let email, let username, let password):
            return .requestParameters(parameters: [
                                        "email": email,
                                        "username": username,
                                        "password": password],
                                      encoding: JSONEncoding.default)
            
        }
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        switch self {
        case .login, .otpValidation, .signup:
            return  [
                "Content-Type": "application/json"
            ]
        case .refreshToken:
            return [
                "Content-Type": "application/json",
                "Authorization": "bearer \(token)"
            ]
        }
    }
    
    
}
