//
//  AuthNetworkManager.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation

public protocol AuthNetworkManager {
    func login(email: String, password: String, completion: @escaping (LoginResponse?, Error?) -> ())
    func otpValidation(email: String, otp: String, completion: @escaping (OtpResponse?, Error?) -> ())
    func refreshToken(email: String, token: String, completion: @escaping (RefreshTokenDataResponse?, Error?) -> ())
    func signup(email: String, username: String, password: String, completion: @escaping (SignupResponse?, Error?) -> ())
}
