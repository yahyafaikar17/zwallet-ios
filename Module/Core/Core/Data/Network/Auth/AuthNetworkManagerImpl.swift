//
//  AuthNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Moya

public class AuthNetworkManagerImpl: AuthNetworkManager {
    
    public init() {
        
    }
    
    public func login(email: String, password: String, completion: @escaping (LoginResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>()
        provider.request(.login(email: email, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let loginResponse = try decoder.decode(LoginResponse.self, from: result.data)
                    completion(loginResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func otpValidation(email: String, otp: String, completion: @escaping (OtpResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>()
        provider.request(.otpValidation(email: email, otp: otp)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let otpResponse = try decoder.decode(OtpResponse.self, from: result.data)
                    completion(otpResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func refreshToken(email: String, token: String, completion: @escaping (RefreshTokenDataResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>()
        provider.request(.refreshToken(email: email, refreshToken: token)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let refreshToken =  try decoder.decode(RefreshTokenResponse.self, from: result.data) as RefreshTokenResponse
                    completion(refreshToken.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func signup(email: String, username: String, password: String, completion: @escaping (SignupResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>()
        provider.request(.signup(email: email, username: username, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let signupResponse = try decoder.decode(SignupResponse.self, from: result.data)
                    completion(signupResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
