//
//  SignupResponse.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public class SignupResponse: Codable {
    public var status: Int
    public var message: String
}
