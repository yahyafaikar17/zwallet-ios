//
//  TransferNetworkManager.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public protocol TransferNetworkManager {
    func findAllContacts(completion: @escaping ([ContactDataResponse]?, Error?) -> ())
    func findContactByName(name: String, completion: @escaping ([ContactDataResponse]?, Error?) -> ())
    func performTransfer(id: Int, amount: Int, notes: String, pin: String, completion: @escaping (TransferResponse?, Error?) -> ())
}
