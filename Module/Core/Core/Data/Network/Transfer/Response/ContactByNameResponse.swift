//
//  ContacyByNameResponse.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public struct ContactByNameResponse: Codable {
    public var status: Int
    public var message: String
    public var data: [ContactDataResponse]
}
