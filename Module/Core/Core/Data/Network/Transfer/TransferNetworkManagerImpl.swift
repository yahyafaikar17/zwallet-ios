//
//  TransferNetworkManagerApi.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public class TransferNetworkManagerImpl: TransferNetworkManager {
    
    public init() {
        
    }
    
    public func findAllContacts(completion: @escaping ([ContactDataResponse]?, Error?) -> ()) {
        let provider = MoyaProvider<TransferApi>(isRefreshToken: true)
        provider.request(.searchAllContacts) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getContactResponse = try decoder.decode(ContactResponse.self, from: result.data)
                    completion(getContactResponse.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func findContactByName(name: String, completion: @escaping ([ContactDataResponse]?, Error?) -> ()) {
        let provider = MoyaProvider<TransferApi>(isRefreshToken: true)
        provider.request(.searchContactByName(name: name)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getContactByName =  try decoder.decode(ContactByNameResponse.self, from: result.data)
                    completion(getContactByName.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func performTransfer(id: Int, amount: Int, notes: String, pin: String, completion: @escaping (TransferResponse?, Error?) -> ()) {
        let prodiver = MoyaProvider<TransferApi>(isRefreshToken: true)
        prodiver.request(.newTransfer(id: id, amount: amount, notes: notes, pin: pin)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let transfer = try decoder.decode(TransferResponse.self, from: result.data)
                    completion(transfer, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
