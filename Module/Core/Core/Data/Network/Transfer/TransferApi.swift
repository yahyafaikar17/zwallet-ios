//
//  TransferApi.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public enum TransferApi {
    case searchAllContacts
    case searchContactByName(name: String)
    case newTransfer(id: Int, amount: Int, notes: String, pin: String)
}

extension TransferApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var path: String {
        switch self {
        case .searchAllContacts:
            return "/tranfer/contactUser"
        case .searchContactByName:
            return "/tranfer/search"
        case .newTransfer:
            return "/tranfer/newTranfer"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .searchAllContacts:
            return .get
        case .searchContactByName :
            return .get
        case .newTransfer:
            return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .searchAllContacts:
            return .requestPlain
        case .searchContactByName(let name):
            return .requestParameters(parameters: ["name": name], encoding: URLEncoding.queryString)
        case .newTransfer(let id, let amount, let notes, _):
            return .requestParameters(parameters: ["receiver": id, "amount": amount, "notes": notes], encoding: JSONEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        switch self {
        case .searchAllContacts, .searchContactByName:
            return ["Content-Type": "application/json",
                    "Authorization": "Bearer \(token)"
            ]
        case .newTransfer(_, _, _, let pin):
            return ["Content-Type": "application/json",
                    "Authorization": "Bearer \(token)",
                    "x-access-PIN": "\(pin)"
            ]
        }
    }
    
    
}
