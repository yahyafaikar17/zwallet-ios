//
//  StringExtension.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation

public extension String {
    func collectNumberOnly() -> String {
        self.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
    }
}
