//
//  UIButtonExtension.swift
//  Core
//
//  Created by MacBook on 31/05/21.
//

import Foundation
import UIKit

public extension UIButton {
    func disableButton() {
        self.isEnabled = false
        self.backgroundColor = .lightGray
    }
    
    func enableButton() {
        self.isEnabled = true
        self.backgroundColor = UIColor(named: "Primary Color", in: Bundle(identifier: "com.casestudy.App"), compatibleWith: nil)
    }
}
