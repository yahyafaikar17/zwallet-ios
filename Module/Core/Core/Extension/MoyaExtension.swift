//
//  MoyaExtension.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Moya
import UIKit

extension MoyaProvider {
    convenience init(isRefreshToken: Bool) {
        if isRefreshToken {
            self.init(requestClosure: MoyaProvider.endpointResolver())
        } else {
            self.init()
        }
    }
    
    static func endpointResolver() -> MoyaProvider<Target>.RequestClosure {
        return { (endpoint, closure) in
            // Get original request
            let request = try! endpoint.urlRequest()
            
            // existing token saved in userdefault
            let tokenExpiredDate: Date? = UserDefaultHelper.shared.get(key: .userTokenExpired)
            if tokenExpiredDate! > Date() {
                closure(.success(request))
                return
            }
            
            let email: String = UserDefaultHelper.shared.get(key: .email) ?? ""
            let refreshToken: String = UserDefaultHelper.shared.get(key: .refreshToken) ?? ""
            let authnetwork = AuthNetworkManagerImpl()
            authnetwork.refreshToken(email: email, token: refreshToken) { (result, error) in
                if let refreshToken = result {
                    UserDefaultHelper.shared.set(key: .userToken, value: refreshToken.token)
                    
                    let currentDate: Date = Date()
                    // change value same as in api
                    let tokenExpiredAt: Date = Calendar.current.date(byAdding: .second, value: refreshToken.expiredIn / 1000, to: currentDate)!
                    UserDefaultHelper.shared.set(key: .userTokenExpired, value: tokenExpiredAt)
                    // update new token to header authorization
                    let newEndpoint = endpoint.adding(newHTTPHeaderFields: ["Authorization": "bearer \(refreshToken.token)"])
                    
                    let finalRequest = try! newEndpoint.urlRequest()
                    closure(.success(finalRequest))
                } else {
                    UserDefaultHelper.shared.remove(key: .email)
                    UserDefaultHelper.shared.remove(key: .userToken)
                    UserDefaultHelper.shared.remove(key: .userTokenExpired)
                    let alert = UIAlertController(title: "Sesi Telah Berakhir", message: "Sesi anda telah berakhir, mohon untuk login kembali", preferredStyle: .alert)
                    alert.addAction(.init(title: "OK", style: .default, handler: nil))
//                    present
                    NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
                }
            }
            
        }
    }
}
