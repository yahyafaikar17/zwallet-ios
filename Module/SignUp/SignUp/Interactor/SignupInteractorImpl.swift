//
//  SignupInteractorImpl.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

class SignupInteractorImpl: SignupInteractor {
    
    var interactorOutput: SignupOutputInteractor?
    var signupNetworkManager: AuthNetworkManager
    
    init(signupNetworkManager: AuthNetworkManager) {
        self.signupNetworkManager = signupNetworkManager
    }
    
    func postSignupData(email: String, username: String, password: String) {
        self.signupNetworkManager.signup(email: email, username: username, password: password, completion: { (data, error) in
            
            if data?.status == 401 {
//                print(signupData)
                self.interactorOutput?.signupResult(isSuccess: false)
            } else {
                self.interactorOutput?.signupResult(isSuccess: true)
            }
        }
    )}
}
