//
//  SignupApi.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Moya
import Core

public enum SignupApi {
    case signup(email: String, username: String, password: String)
}

extension SignupApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var path: String {
        switch self {
        case .signup:
            return "/auth/signup"
        }
    }
    
    public var method: Moya.Method {
        return .post
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .signup(let email, let username, let password):
            return .requestParameters(parameters: [
                                        "email": email,
                                        "username": username,
                                        "password": password],
                                      encoding: JSONEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return [
            "Content-Type": "application/json"
        ]
    }
    
    
}
