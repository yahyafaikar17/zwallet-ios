//
//  SignupResponse.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation

public class SignupResponse: Codable {
    public var status: Int
    public var message: String
}
