//
//  LoginInteractor.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol SignupInteractor {
    func postSignupData(email: String, username: String, password: String)
}
