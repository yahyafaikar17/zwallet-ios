//
//  SignupOutputInteractor.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol SignupOutputInteractor {
    func signupResult(isSuccess: Bool)
}
