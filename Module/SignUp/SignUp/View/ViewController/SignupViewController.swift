//
//  SignupViewController.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import UIKit

class SignupViewController: UIViewController {

    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var hidePassword: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var usernameImage: UIImageView!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var emailIamge: UIImageView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordImage: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    
    var presenter: SignupPresenter?
    var bundle = Bundle(identifier: "com.casestudy.Signup")
    var lineBundle = Bundle(identifier: "com.casestudy.App")
    var onClick: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modalPresentationStyle = .fullScreen
        
        signUpButton.disableButton()
        usernameView.addBottomBorderWithColor(color: .lightGray, width: 2)
        passwordView.addBottomBorderWithColor(color: .lightGray, width: 2)
        emailView.addBottomBorderWithColor(color: .lightGray, width: 2)
        
        emailText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        usernameText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        loadingIndicator.isHidden = true
    }
    
    @IBAction func signupAction(_ sender: Any) {
        let username: String = usernameText.text ?? ""
        let email: String = emailText.text ?? ""
        let password: String = passwordText.text ?? ""
        
        self.presenter?.signup(email: email, username: username, password: password)
        loadingIndicator.isHidden = false
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        self.presenter?.navigateToLogin()
    }
    
    
    @IBAction func showPasswordAction(_ sender: Any) {
        if onClick {
            passwordText.isSecureTextEntry = true
            hidePassword.setImage(UIImage(named: "eye-crossed_empty.png", in: bundle, compatibleWith: nil), for: .normal)
            self.onClick = false
//            hidePassword.tag = 1
        } else {
            passwordText.isSecureTextEntry = false
            hidePassword.setImage(UIImage(named: "eye-crossed_filled.png", in: bundle, compatibleWith: nil), for: .normal)
            self.onClick = true
//            hidePassword.tag = 0
        }
    }
    
    func enableSignupButton() {
        if !(usernameText.text?.isEmpty ?? false) && !(passwordText.text?.isEmpty ?? false) && !(emailText.text?.isEmpty ?? false) {
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = .blue
            signUpButton.tintColor = .white
        } else {
            
        }
    }
    
    func filledUsername() {
        if (usernameText.text?.isEmpty ?? true) {
            usernameImage.image = UIImage(named: "person_empty.png", in: bundle, compatibleWith: nil)
            usernameView.addBottomBorderWithColor(color: .lightGray, width: 2)
        } else {
            usernameImage.image = UIImage(named: "person_filled.png", in: bundle, compatibleWith: nil)
            usernameView.addBottomBorderWithColor(color: UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black, width: 2)
        }
    }
    
    func filledEmail() {
        if (emailText.text?.isEmpty ?? true) {
            emailIamge.image = UIImage(named: "mail_empty.png", in: bundle, compatibleWith: nil)
            emailView.addBottomBorderWithColor(color: .lightGray, width: 2)
        } else {
            emailIamge.image = UIImage(named: "mail_filled.png", in: bundle, compatibleWith: nil)
            emailView.addBottomBorderWithColor(color: UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black, width: 2)
        }
    }
    
    func filledPassword() {
        if (passwordText.text?.isEmpty ?? true) {
            passwordImage.image = UIImage(named: "lock_empty.png", in: bundle, compatibleWith: nil)
            passwordView.addBottomBorderWithColor(color: .lightGray, width: 2)
        } else {
            passwordImage.image = UIImage(named: "lock_filled.png", in: bundle, compatibleWith: nil)
            passwordView.addBottomBorderWithColor(color: UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black, width: 2)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filledUsername()
        filledEmail()
        filledPassword()
        if !(emailText.text?.isEmpty ?? false) && !(passwordText.text?.isEmpty ?? false) && !(usernameText.text?.isEmpty ?? false) && emailText.text?.contains("@") ?? true {
            signUpButton.enableButton()
        } else {
            signUpButton.disableButton()
        }
    }
    
}

extension SignupViewController: SignupView {
    func sendEmailToOtp() {
        self.presenter?.navigateToOtp(email: emailText.text ?? "", viewController: self)
    }
    
    func showError() {
        let alert = UIAlertController(title: "Peringatan", message: "Terjadi kesalahan atau email telah digunakan", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.loadingIndicator.isHidden = true
    }
}
