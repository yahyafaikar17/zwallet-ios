//
//  SignupView.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol SignupView {
    func showError()
    func sendEmailToOtp()
}
