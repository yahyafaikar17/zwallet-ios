//
//  SignupRouter.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import UIKit

protocol SignupRouter {
    func navigateToOtp(viewController: UIViewController, email: String)
    func navigateToLogin()
}
