//
//  SignupRouterImpl.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core
import UIKit

public class SignupRouterImpl {
    public static func navigateToModule(view: UIViewController) {
        let bundle = Bundle(identifier: "com.casestudy.Signup")
        let vc = SignupViewController(nibName: "SignupViewController", bundle: bundle)
        
        let networkManager = AuthNetworkManagerImpl()
        
        let router = SignupRouterImpl()
        let interactor = SignupInteractorImpl(signupNetworkManager: networkManager)
        let presenter =  SignupPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        vc.modalPresentationStyle = .fullScreen
        view.present(vc, animated: true, completion: nil)
    }
}

extension SignupRouterImpl: SignupRouter {
    func navigateToOtp(viewController: UIViewController, email: String) {
        // To OTP Confirmation
        AppRouter.shared.navigateToOTP(viewController, email: email)
    }
    
    func navigateToLogin() {
        AppRouter.shared.navigationToLogin()
    }
}
