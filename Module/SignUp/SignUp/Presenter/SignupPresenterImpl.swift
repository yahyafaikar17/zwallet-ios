//
//  SignupPresenterImpl.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core
import UIKit

class SignupPresenterImpl: SignupPresenter {
    
    let view: SignupView
    let interactor: SignupInteractor
    let router: SignupRouter
    
    init(view: SignupView, interactor: SignupInteractor, router: SignupRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func signup(email: String, username: String, password: String) {
        self.interactor.postSignupData(email: email, username: username, password: password)
    }
    
    func navigateToLogin() {
        self.router.navigateToLogin()
    }
    
    func navigateToOtp(email: String, viewController: UIViewController) {
        self.router.navigateToOtp(viewController: viewController, email: email)
    }
}

extension SignupPresenterImpl: SignupOutputInteractor {
    func signupResult(isSuccess: Bool) {
        if isSuccess {
//            let vc =
            self.view.sendEmailToOtp()
        } else {
            self.view.showError()
        }
    }
}
