//
//  SignupPresenter.swift
//  Signup
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import UIKit

protocol SignupPresenter {
    
    func signup(email: String, username: String, password: String)
    func navigateToLogin()
    func navigateToOtp(email: String, viewController: UIViewController)
}
