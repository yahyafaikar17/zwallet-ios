//
//  CreatePinInteractorImpl.swift
//  CreatePin
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

class CreatePinInteractorImpl: CreatePinInteractor {
    
    var interactorOutput: CreatePinInteractorOutput?
    let pinNetworkManager: PinNetworkManager
    
    init(pinNetworkManager: PinNetworkManager) {
        self.pinNetworkManager = pinNetworkManager
    }
    
    func patchPinData(pin: String) {
        self.pinNetworkManager.create(pin: pin) { (data, error) in
            
            if data?.status == 200 {
//                print(pinData.message + pinData.status)
                self.interactorOutput?.checkPin(isTrue: true)
            } else {
                self.interactorOutput?.checkPin(isTrue: false)
            }
        }
    }
}
