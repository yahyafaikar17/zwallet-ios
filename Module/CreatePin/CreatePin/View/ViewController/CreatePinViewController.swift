//
//  CreatePinViewController.swift
//  CreatePin
//
//  Created by MacBook on 27/05/21.
//

import UIKit

class CreatePinViewController: UIViewController {

    @IBOutlet weak var inputPinText: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var createPinView: UIView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var pinText: UITextField!
    @IBOutlet weak var loadingIndicator: UIView!
    
    var presenter: CreatePinPresenter?
    var lineBundle = Bundle(identifier: "com.casestudy.App")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.successView.isHidden = true
        
        confirmButton.disableButton()
        pinText.borderColor = .lightGray
        pinText.borderWidth = 2
        pinText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        loadingIndicator.isHidden = true
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        self.presenter?.create(pin: pinText.text ?? "")
        loadingIndicator.isHidden = false
    }
    
    @IBAction func loginNowAction(_ sender: Any) {
        self.presenter?.navigateToHome()
    }
    
    func filledPIN() {
        if pinText.text?.count ?? 0 == 6 {
            pinText.borderColor = UIColor(named: "Primary Color", in: lineBundle, compatibleWith: nil) ?? .black
            confirmButton.enableButton()
        } else {
            pinText.borderColor = .lightGray
            confirmButton.disableButton()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filledPIN()
    }
    
}

extension CreatePinViewController: CreatePinView {
    func showPinError() {
        let alert = UIAlertController(title: "Gagal", message: "Gagal Set PIN", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.loadingIndicator.isHidden = true
    }
    
    func showPinSuccess() {
        loadingIndicator.isHidden = true
        self.successView.isHidden = false
        self.createPinView.isHidden = true
        self.loadingIndicator.isHidden = true
    }
    
    
}
