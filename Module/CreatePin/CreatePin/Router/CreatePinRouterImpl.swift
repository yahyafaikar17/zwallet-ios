//
//  CreatePinRouterImpl.swift
//  CreatePin
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

public class CreatePinRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.casestudy.CreatePin")
        let vc = CreatePinViewController(nibName: "CreatePinViewController", bundle: bundle)
        
        let createPinNetworkManager = PinNetworkManagerImpl()
        
        let router = CreatePinRouterImpl()
        let interactor = CreatePinInteractorImpl(pinNetworkManager: createPinNetworkManager)
        let presenter = CreatePinPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
//        vc.modalPresentationStyle = .fullScreen
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
//        view.present(vc, animated: true, completion: nil)
    }
    
}

extension CreatePinRouterImpl: CreatePinRouter {
    func navigateToHome() {
        AppRouter.shared.navigationToHome()
    }
    
    func navigateToPinSuccess() {
        
    }
}
