//
//  CreatePinPresenter.swift
//  CreatePin
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol CreatePinPresenter {
    func create(pin: String)
    func navigateToHome()
}
