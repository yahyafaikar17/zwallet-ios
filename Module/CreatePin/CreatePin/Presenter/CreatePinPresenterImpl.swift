//
//  CreatePinPresenterImpl.swift
//  CreatePin
//
//  Created by MacBook on 27/05/21.
//

import Foundation

class CreatePinPresenterImpl: CreatePinPresenter {
    
    
    
    let view: CreatePinView
    let interactor: CreatePinInteractor
    let router: CreatePinRouter
    
    init(view: CreatePinView, interactor: CreatePinInteractor, router: CreatePinRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func create(pin: String) {
        self.interactor.patchPinData(pin: pin)
    }
    
    func navigateToHome() {
        self.router.navigateToHome()
    }
}

extension CreatePinPresenterImpl: CreatePinInteractorOutput {
    func checkPin(isTrue: Bool) {
        if isTrue {
            self.view.showPinSuccess()
//            self.router.navigateToHome()
        } else {
            self.view.showPinError()
        }
    }
}
