//
//  ReceiverInteractorImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

class ReceiverInteractorImpl: ReceiverInteractor {
    
    var interactorOutput: ReceiverOutputInteractor?
    
    let receiverNetworkManager: TransferNetworkManager
    
    init(receiverNetworkManager: TransferNetworkManager) {
        self.receiverNetworkManager = receiverNetworkManager
    }
    
    func getAllContacts() {
        self.receiverNetworkManager.findAllContacts { (data, error) in
            var contacts: [ContactEntity] = []
            data?.forEach({ (contactData) in
                contacts.append(ContactEntity(id: contactData.id, name: contactData.name, phoneNumber: contactData.phone, image: "\(AppConstant.baseUrl)\(contactData.image)"))
                
                self.interactorOutput?.loadedAllContacts(contacts: contacts)
            })
        }
    }
    
    func getContactByName(name: String) {
        self.receiverNetworkManager.findContactByName(name: name) { (data, error) in
            var contactByName: [ContactEntity] = []
            data?.forEach({ (contactData) in
                contactByName.append(ContactEntity(id: contactData.id, name: contactData.name, phoneNumber: contactData.phone, image: "\(AppConstant.baseUrl)\(contactData.image)"))
                self.interactorOutput?.loadedContactByName(contacts: contactByName)
            })
        }
    }
    
    func getQuickAccessContact() {
        // Get Quick Access Contacts
    }
}
