//
//  ReceiverOutputInteractor.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

protocol ReceiverOutputInteractor {
    func loadedAllContacts(contacts: [ContactEntity])
    func loadedQuickAccess(contacts: [ContactEntity])
    func loadedContactByName(contacts: [ContactEntity])
}
