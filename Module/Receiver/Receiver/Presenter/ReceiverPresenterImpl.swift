//
//  ReceiverPresenterImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

class ReceiverPresenterImpl: ReceiverPresenter {
    
    let view: ReceiverView
    let interactor: ReceiverInteractor
    let router: ReceiverRouter
    
    init(view: ReceiverView, interactor: ReceiverInteractor, router: ReceiverRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadAllContacts() {
        self.interactor.getAllContacts()
    }
    
    func loadQuickAccess() {
        // Load QuickContacts
    }
    
    func backToHome() {
        self.router.navigateToHome()
    }
    
    func goToInputAmount(viewController: UIViewController, contact: ContactEntity) {
        self.router.navigateToTransfer(viewController: viewController, contact: contact)
    }
    
    func loadContactByName(name: String) {
        self.interactor.getContactByName(name: name)
    }
}

extension ReceiverPresenterImpl: ReceiverOutputInteractor {
    func loadedContactByName(contacts: [ContactEntity]) {
        self.view.showContactsByName(contacts: contacts)
    }
    
    func loadedAllContacts(contacts: [ContactEntity]) {
        self.view.showAllContacts(contacts: contacts)
    }
    
    func loadedQuickAccess(contacts: [ContactEntity]) {
        // Loaded Quick Access Contacts
    }
    
    
}
