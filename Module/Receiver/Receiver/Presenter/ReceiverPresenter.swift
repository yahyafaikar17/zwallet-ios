//
//  ReceiverPresenter.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

protocol ReceiverPresenter {
    func loadAllContacts()
    func loadContactByName(name: String)
    func loadQuickAccess()
    func backToHome()
    func goToInputAmount(viewController: UIViewController, contact: ContactEntity)
}
