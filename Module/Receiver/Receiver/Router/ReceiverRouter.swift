//
//  ReceiverRouter.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

protocol ReceiverRouter {
    func navigateToHome()
    func navigateToTransfer(viewController: UIViewController, contact: ContactEntity)
}
