//
//  ReceiverRouterImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class ReceiverRouterImpl {
    
    public static func navigateToModule(view: UIViewController) {
        let bundle = Bundle(identifier: "com.casestudy.Receiver")
        let vc = ReceiverViewController(nibName: "ReceiverViewController", bundle: bundle)
        
        let receiverNetworkManager = TransferNetworkManagerImpl()
        let router = ReceiverRouterImpl()
        let interactor = ReceiverInteractorImpl(receiverNetworkManager: receiverNetworkManager)
        let presenter = ReceiverPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        vc.modalPresentationStyle = .fullScreen
        
        view.present(vc, animated: true, completion: nil)
    }
}

extension ReceiverRouterImpl: ReceiverRouter {
    func navigateToHome() {
        AppRouter.shared.navigationToHome()
    }
    
    func navigateToTransfer(viewController: UIViewController, contact: ContactEntity) {
//        To Input Amount
        AppRouter.shared.navigateToTransfer(viewController, contact: contact)
    }
}
