//
//  ReceiverViewController.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core

class ReceiverViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var dataSource = ReceiverDataSource()
    var presenter: ReceiverPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.presenter?.loadAllContacts()
        self.searchBar.delegate = self
//        self.
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "QuickAccessCell", bundle: Bundle(identifier: "com.casestudy.Receiver")), forCellReuseIdentifier: "QuickAccessCell")
        self.tableView.register(UINib(nibName: "ContactsTiltleCell", bundle: Bundle(identifier: "com.casestudy.Receiver")), forCellReuseIdentifier: "ContactsTiltleCell")
        self.tableView.register(UINib(nibName: "ContactCell", bundle: Bundle( identifier: "com.casestudy.Core")), forCellReuseIdentifier: "ContactCell")
        self.tableView.dataSource = self.dataSource
    }
    
    @IBAction func backToHomeAction(_ sender: Any) {
        self.presenter?.backToHome()
    }
}

extension ReceiverViewController: ReceiverView {
    func showContactsByName(contacts: [ContactEntity]) {
        self.dataSource.contacts = contacts
        self.tableView.reloadData()
    }
    
    func showAllContacts(contacts: [ContactEntity]) {
        self.dataSource.contacts = contacts
        self.loadingIndicator.isHidden = true
        self.tableView.reloadData()
    }
}

extension ReceiverViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchBar.text!.isEmpty {
            self.presenter?.loadContactByName(name: searchBar.text ?? "")
            self.tableView.reloadData()
        } else {
            self.presenter?.loadAllContacts()
        }
    }
}

extension ReceiverViewController: ContactCellDelegate {
    
    func navigateToTransfer(contact: ContactEntity) {
        self.presenter?.goToInputAmount(viewController: self, contact: contact)
    }
}
