//
//  ReceiverDataSource.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

class ReceiverDataSource: NSObject, UITableViewDataSource {
    
    var viewController: ReceiverViewController!
    var contacts: [ContactEntity] = []
    var selectedContact: ContactEntity = ContactEntity(id: 0, name: "", phoneNumber: "", image: "")
//    public var filteredContacts: [ContactEntity] = []
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            return 1
        } else {
            return contacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuickAccessCell", for: indexPath) as! QuickAccessCell
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTiltleCell", for: indexPath) as! ContactsTiltleCell
            cell.numberOfContactLabel.text = "\(contacts.count) Contact Founds"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
            cell.showContactData(contact: self.contacts[indexPath.row])
            selectedContact = self.contacts[indexPath.row]
            cell.delegate = viewController
            return cell
        }
    }
    
    
}
